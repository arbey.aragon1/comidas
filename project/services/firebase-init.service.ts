import { app, initializeApp } from "firebase";
import { firebaseConfig } from "../configuration/firebase.config";

export const usrApp: app.App = initializeApp(firebaseConfig);
