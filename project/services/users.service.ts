import { usrApp } from "./firebase-init.service";
import { User } from "../models/user";
import * as constants from "../shared/constants";

export class UsersService {
  private rd: firebase.database.Database;

  constructor() {
    this.rd = usrApp.database();
  }

  createUser(
    email: string,
    pass: string
  ): Promise<firebase.auth.UserCredential> {
    return usrApp.auth().createUserWithEmailAndPassword(email, pass);
  }

  setUser(uid: string, user: User): Promise<any> {
    return this.rd
      .ref(constants.USERS)
      .child(uid)
      .set(user);
  }
}
