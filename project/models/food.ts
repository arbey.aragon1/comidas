export interface InterfaceFood {
  description: string;
}
export class Food {
  public static attributes: InterfaceFood = {
    description: "description"
  };

  constructor(private _key: string, private _description: string) {}

  public get key() {
    return this._key;
  }

  public get description() {
    return this._description;
  }

  public toJson(): object {
    return {
      description: this.description
    };
  }

  public static fromJson(data: any): Food {
    return new Food(data.key, data.description);
  }

  public static fromJsonArray(array: any): Food[] {
    return array.map(Food.fromJson);
  }
}
