export interface InterfaceUser {
  email: string;
}
export class User {
  public static attributes: InterfaceUser = {
    email: "email"
  };

  constructor(private _key: string, private _email: string) {}

  public get key() {
    return this._key;
  }

  public get email() {
    return this._email;
  }

  public toJson(): object {
    return {
      email: this.email
    };
  }

  public static fromJson(data: any): User {
    return new User(data.key, data.email);
  }

  public static fromJsonArray(array: any): User[] {
    return array.map(User.fromJson);
  }
}
