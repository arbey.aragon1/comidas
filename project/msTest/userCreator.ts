import { UsersService } from "../services/users.service";
import { User } from "../models/user";

let test: UsersService = new UsersService();

test
  .createUser("test3@test.com", "123456789")
  .then(data => {
    console.log(data.user.uid);
    let user: User = new User(data.user.uid, data.user.email);

    test.setUser(data.user.uid, user).then(() => {
      console.log("Usuario creado");
    });
  })
  .catch((er: any) => {
    console.log("error");
  });
