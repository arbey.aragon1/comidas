import { app, initializeApp } from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyARNRSOYgpvBMmwPfBwSs8mGQw__FszX6M",
  authDomain: "frontdb-3bfde.firebaseapp.com",
  databaseURL: "https://frontdb-3bfde.firebaseio.com",
  projectId: "frontdb-3bfde",
  storageBucket: "frontdb-3bfde.appspot.com",
  messagingSenderId: "262396219357"
};

export const usrApp: app.App = initializeApp(firebaseConfig);
const referenceDatabase = usrApp.database();

const query: firebase.database.Query = referenceDatabase.ref("items");

query.once("value", (snapshot: any) => {
  let valueTem: any = snapshot.val();
  console.log(snapshot.val());
});

let dataToSave: any = {};

dataToSave[`jaja/jojo`] = { name: "Test1", description: "test1" };
dataToSave[`jaja/joju`] = { name: "Test1", description: "test1" };
dataToSave[`jajo/joju`] = { name: "Test1", description: "test1" };

referenceDatabase
  .ref()
  .update(dataToSave)
  .then(() => {
    console.log("Creados..");
  });
