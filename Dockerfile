FROM node:latest
MAINTAINER ARBEY arbey.aragon@gmail.com

RUN apt-get update && apt-get install -y
RUN npm install -g ts-node typescript 
RUN npm install -g @nestjs/cli

EXPOSE 4200
CMD bash
